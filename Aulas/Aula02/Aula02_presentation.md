<!--
theme: default
paginate: false
-->

# Aula 02 - Programando com JavaScript

---

<!-- paginate: true -->

## Conceitos

O JavaScript é uma linguagem de programação dinâmica que foi criada por Brendan Eich, também co-fundador do projeto Mozilla.

A flexibilidade do JavaScript permitiu que ele fosse utilizado em diversos ambientes, que vão muito além da interação dinâmica com páginas HTML, pro qual foi criado.

Atualmente, o JavaScript pode ser utilizado para construir aplicações para diversos dispositivos (deskop, mobile, web...) e diversos ecosistemas.

---

### ECMA

O JavaScript é padronizado pela ECMA International, uma organização para padronização de sistemas de comunicação. Essa padronização é publicada através de especificações, que por sua vez são utilizadas por empresas e projetos para projetar suas próprias implementações do javascript.

## Programando

Apesar de ser utilizado de diversas maneiras, o código escrito em JavaScript tem sua sintaxe preservada em todo tipo de aplicação.

As próximas seções apresentaram uma forma de vincular o código JavaScript em sua aplicação "nativa": páginas HTML.

---

### Vinculando ao HTML

Para começar a escrever código JavaScript em uma página HTML, podemos de forma simples utilizar o elemento ``script`` para criar um novo script no mesmo documento ou apontar para um documento JavaScript externo.

````html
<script src="olamundo.js"></script>

ou

<script>
    //Seu código aqui!
</script>
````

---

### Variáveis

Declaração:

````javascript
var minhaVariavel;
````

Atribuição:

````javascript
minhaVariavel = 500;
````

> Ponto e virgula são necessários apenas para separar instruções em uma mesma linha

> O JavaScript é case sensitive

---

### Tipos de Dados

Apesar de ser uma linguagem de tipagem dinâmica (os tipos de dados são automaticamente determinados), o JavaScript possui sete tipos de dados padrão:

* Boolean
* Null
* Undefined
* Number
* String
* Array
* Object

> Veja mais em [Mozilla - JavaScript Data Structures](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Data_structures)

---

Exemplos:

````javascript
//bool
var chovendo = false
//null
var inimigo = null
//undefined
var casa
//number
var pi = 3.14
//string
var nome = "ramon"
//array
var lista = ["ramon", 0, null]
//object
var data = {
    serie: 1,
    curso: "computacao"
}
````

---

### Comentários

````javascript
// Comentário de linha
/*
    Comentário de bloco
*/
````

---

### Operadores

Operadores matemáticos:

````javascript
5 + 3 //soma
10 - 5 //subtração
8 * 2 //multiplicação
27 / 9 // divisão
12 % 2 // módulo
````

---

Operadores lógicos:

````javascript
= //atribuição
== //igualdade
=== //igualdade estrita
! // negacação
````

> Enquanto o operador de igualdade não leva em consideração o tipo de dado comparado, o operador de igualdade estrita retorna verdadeiro apenas se o conteúdo **e** tipo de ambos os dados são idênticos.

> A negação de igualdade e negação estrita é realiza respectivamente com ``!=`` e ``!==``

---

### Condicionais

IF/ELSE

````javascript
if (condicao) {
  //caso verdadeiro
} else {
  //caso falso  
}
````

---

### Repetições

* FOR

````javascript
for (var index = 0; index < 50; index++) {
    //iteração
}
````

---

* FOR-In

````javascript
for(var item in objeto){
    //iteração
}
````

---

* FOR-of

````javascript
for (var iterador of vetor) {
    //iteração
}
````

---

* WHILE

````javascript
while(condicao) {
    //iteração
}
````

---

### Declaração de Funções

````javascript
function nomeDaFuncao(parametro){
    //ações
}

//para chamara função, basta utilizar
nomeDaFuncao(parametro)
````

---

### Funções Pré-Definidas

O Javascript possui algumas funções pré-defindas:

````javascript
parseInt(texto) // analisa string e retorna um inteiro
parseFloat(texto) // analisa string e retorna flutuante
console.log(texto) //imprime no console
console.error(texto) //imprime um erro no console
prompt(texto) // Aguarda entrada de dados no console
````

---

#### Acessando o DOM

A interface ``document`` do javascript serve como uma porta de acesso para o conteúdo da página HTML. Dessa forma é possivel acessar, criar e alterar os elementos da árvore DOM no documento ao qual o código javascript encontra-se inserido.

````javascript
document.activeElement // retorna o elemento ativo
document.body // retorna o elemento body do documento
document.URL // retorna a URL do documento atual
document.createElement(elemento) // cria um novo elemento html
document.createTextNode(texto) // cria um bloco de texto
novoElemento.textContent = texto // atualiza o bloco de texto de um elemento
document.body.appendChild(elemento) // insere elemento no body
document.getElementsByTagName(elemento) // retorna lista de elmentos com o nome
document.getElementsByClassName(classe) // retorna lista de elmentos com atributo classe
document.getElementById(id) // retorna elemento com o id
elemento.remove() // remove o elemento
````

> Documentação do [document](https://developer.mozilla.org/pt-BR/docs/Web/API/Document)

---

#### Eventos

````javascript
window.addEventListener('click', callback) // adiciona um callback para evento click
formulario.addEventListener('submit', callback) //adiciona um callback para o evento submit em um elemento form
````

> Documentação de [eventos](https://developer.mozilla.org/pt-BR/docs/Web/Event)

---

#### Outras Funções

````javascript
alert(texto) // exibe um alerta no navegador com o texto no parametro
setTimeout(callback, tempo) // define um tempo de espera para executar a função callback
````

---

### Callbacks

Funções callback no javascript são funções passadas como argumento para outra função.

````javascript
function callbackChuva(){
    console.log('Está chovendo')
}

function callbackSol(){
    console.log('Está fazendo sol')
}

function previsao(callback){
    callback()
}

previsao(callbackSol)

````

---

Este é um exemplo de callback executado de forma síncrona, ou seja, a sequência das funções é executada exatamente na ordem em que são chamadas. No entanto, a utilização de callbacks permite a criação de operações assíncronas.

Considere o código abaixo:

````js
function resultado(){
    console.log('Finalizado')
}

console.log(1)
setTimeout(resultado, 1000)
console.log(2)
console.log(3)
````

Qual seria a saída esperada?

---

````
1
2
3
"Finalizado"
````

Dessa forma, a callback pode ser considerada como uma função que é executada apenas quando seu resultado da função que à chamou estiver disponível.

---

## Exercícios

5. Crie um formulário com dois campos e um botão capaz de inserir itens e seus respectivos preços em uma lista de compras. Deve-se mostrar o ``valor total`` da compra e, caso ultrapasse 50 reais, a cor da fonte do ``valor total`` deve ser estilizado em vermelho.
6. Crie uma página que permita ao usuário gerenciar uma lista com filmes/jogos. O formulário de inserção deve conter nome, estilo e URL da foto (local ou remota). Cada filme/jogo adicionado deve estar dentro de uma div, e deve apresentar os dados de entrada de forma estilizada (Ex.: a foto deve ser exibida com a tag ``img``).
7. Utilizando como base o ``exercício 6``, implemente também a deleção e atualização dos dados de entrada.

---

8. Crie uma pokédex contendo uma objeto com pelo menos 10 pokémons. O objeto deve conter número, nome, tipo e descrição e uma URL (local ou remota) com a imagem do pokémon. O usuário pode excluir, modificar ou inserir novos pokémons (com todas as suas informações). Ao final, deve-se apresentar um botão ``Showcase`` que exibe imagem e nome dos pokémons a cada 3 segundos. Utilize CSS para estilizar o projeto à sua maneira =)

---

## Programando 2.0

---

### Try-Catch

Para tratamento de erros, é possível usar o try-catch (usa-se o o ``throw new Error(mensagem)`` para especificar um novo erro):

````javascript
function pdm(){
    throw new Error('Esta é uma mensagem de erro')
}

try {
    // código a ser tratado
    pdm()
} catch(erro){
    // Quando um erro acontece
    console.log(erro.message)
}
````

---

## Exercícios

9. Utilizando como base o exercício 04, reestruture o script para que os erros sejam tratados com instruções try-catch. O script deve implementar uma função chamada ``checarPersonagem``, e retornar um erro caso não passe pela validação.
10. Crie uma função que seja capaz de contar todos os objetos de uma classe no DOM. A função deve retornar um erro caso o número de elementos daquele tipo seja maior do que um valor limite passado como parâmetro:

Ex.:

````JavaScript
function contaElementos(elemento, limite){
  // caso o numero de elementos seja maior do que o LIMITE, retornar um erro
}
````

---

### Manipulação de Tipos

---

#### Strings

````javascript
minhaString.length // Retorna quantidade de caracteres
minhaString.split(';') // divide a string no caracter repassado
minhaString.slice(inicio, fim) // retorna uma nova string na posicao informada
minhaString.toUpperCase() // retorna a string em maiusculas
minhaString.toLowerCase() // retorna a string em minusculas
minhaString.trim() // retira espaços em branco do inicio e do fim
minhaString.replace('a', 'b') // substitui as ocorrencias em uma string
minhaString.match(/uva/g) // retorna um array com as ocorrências do regex
````

> A função replace substitui apenas a primeira ocorrência no texto. Para substituir todas as ocorrências, utilize expressões regulares. Mais informações em [Replace](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/String/replace). Sobre [expressões regulares](https://aurelio.net/regex/).

> Outras informações na [documentação](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Guide/Formatando_texto)

---

#### Números

````javascript
Number.MAX_VALUE // Retorna valor máximo
Number.MIN_VALUE // Retorna valor mínimo
Number.POSITIVE_INFINITY; //Infinito positivo
Number.NEGATIVE_INFINITY; //Infinito negativo
Number.NaN; // não é numeral
isNaN(2.2) // verifica se não é número
Math.min(2,4,3) // retorna o menor numero
Math.max(2,4,3) // retorna o maior numero
Math.random() // retorna um número aleatório entre 0 e 1
````

> Mais informações e [Object Number](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Guide/Numeros_e_datas#Objeto_Number)

---

#### Date

O JavaScript possui um tipo de objeto chamado ``Date``:

````javascript
var aniversario = new Date('11/25/1992')
var aniversario = new Date('08:25')
var aniversario = new Date('2012')
````

---

Para recuperar ou atualizar valores específicos, pode-se usar métodos ``get`` e ``set``:

````javascript
var hora = aniversario.getHours() // retorna as horas
var minuto = aniversario.getMinutes() // retorna os minutos
aniversario.setSeconds(20) // atualiza os segundos
````

> Mais informações em [Date](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Guide/Numeros_e_datas#Objeto_Date)

---

## Exercícios

11. Crie um formulário que permita ao usuário digitar um texto longo (elemento ``textarea``), e que retorne em uma ``div`` a quantidade de ocorrências para cada um dos seguintes itens: ``espada``, ``arco`` e ``escudo``
12. Crie um gerador de números da mega-sena. Como referência, o gerador deve apresentar um conjunto de 6 números de 1 a 60, sendo que não há possibilidade de repetir estes números em cada jogada.
13. Utilizando o exercício 12 como referência, apresente também a possibilidade do usuário de comprar um ticket com 6 números, junto com seu nome completo. Ao realizar o sorteio, a página deverá apresentar se há algum ticket ganhador.
14. Utilizando um javascript, crie três listas drop-down (tag ``select``) para apresentar ano, meses e dias. Apenas anos entre 1990 e 1999 devem ser apresentados. Os meses devem estar entre Fevereiro e Agosto. E os dias do mês devem estar entre 05 e 25.
