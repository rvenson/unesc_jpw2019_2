# Aula 04 - Controle de Versão (GIT)

## Git

O [Git](https://git-scm.com/downloads) é um sistema de controle de versão, criado pelo finlandês Linus Torvalds (o mesmo criador do Linux). O sistema Git é utilizado principalmente nos projetos de desenvolvimento de software, sendo capaz de manter a organização das edições de código, além de permitir que diversos pessoas trabalhem simultaneamente em um mesmo projeto.

### O que é o controle de versão?

* Gerenciamento do código-fonte
* Contribuições de diferentes programadores
* Desfazer alterações problemáticas
* Resolução de conflitos de código

### Instalação do Git

#### Windows

1. Faça o download do instalador para windows no [site](https://gitforwindows.org/) do Git.
2. Prossiga com a instalação nas opções padrão. Para usuários avançados, há a possibilidade de alterar algumas opções.
3. Inicie um Git Bash, recém instalado.
4. Execute os seguintes comandos:
```
$ git config --global user.name "Alan Turing"
$ git config --global user.email "alan@turing.com"
```
#### GNU/Linux

1. Instale o git a partir do repositório de sua distribuição (ex.: em distribuições debian)
```
$ sudo apt-get update
$ sudo apt-get install git
```
2. Execute os seguintes comandos a partir do bash:
```
$ git config --global user.name "Alan Turing"
$ git config --global user.email "alan@turing.com"
```

> Os comandos acima configuram o nome e conta de email do usuário que fara contribuições na máquina. Caso compartilhe a máquina com outros usuários, realize a configuração por repositório digitando o mesmo comando sem a opção --global dentro da pasta do repositorio.

## Áreas do Git

![bg fit](../../Assets/img/git-areas.png)

* **STASH**: Área alternativa de armazenamento de alterações.
* **WORKSPACE**: Área de trabalho que mantém o estado real/atual de todos os arquivos do projeto.
* **STAGE AREA (INDEX)**: Área de trabalho intermediária para organização das modificações a serem armazenadas no repositório local através do commit.
* **LOCAL REPOSITORY**: Repositório de todas as versões geradas pelo comando commit. Esta área representa o estado mais estável do desenvolvimento.
* **REMOTE REPOSITORY**: Repositório remoto que reune as contribuições dos repositórios locais.

## Estágios de arquivo

![bg fit](../../Assets/img/git-files.png)

* **UNTRACKED**: Arquivo que não está sendo versionado.
* **UNMODIFIED**: Arquivo que não foi modificado quando comparado à ultima versão. 
* **MODIFIED**: Arquivo que foi modificado quando comparado à ultima versão.
* **STAGED**: Arquivo alterado que foi adicionado ao INDEX e está pronto para o versionamento.

## Comandos

### Navegação Básica
Há duas maneiras distintas de iniciar o terminal de comandos do Git:

1. Através da interface gráfica, entre na pasta onde deseja criar/abrir o repositório do Git e selecione a opção ***Git Bash Here***.

2. Abra a aplicação **Git Bash** e navegue até a pasta desejada usando o comando ``cd <caminho da pasta>`` (Ex.: ``cd Downloads/pascalzim6031/pascalzin``)

### Navegação no Bash
Para navegar no bash, utilize os seguintes comandos como referência:

| Comando | Descrição |
|--|--|
| ``cd <caminho>`` | Acessa a pasta do caminho |
| ``ls`` | Mostra arquivos no diretório atual |
| ``pwd`` | Mostra o caminho do diretório atual |

## Iniciando novo repositório
Para iniciar um novo repositório Git, basta executar os seguintes passos:

1. Certifique-se de que está na pasta correta no bash usando o comando *pwd* ou consultado a barra de título
2. Execute o comando: ``git init``

> NOTA: Repare que é possível iniciar um novo repositório em qualquer pasta que não possua um repositório ativo. Isso inclui pastas que já possuem conteúdo.

## Adicionando alterações
Após adicionar novos arquivos ou realizar alterações nos arquivos existentes, utilize o comando abaixo para adicionar estas alterações na *staging area* do repositório.

````
git add .
````

> NOTA: O ponto no final refere-se ao diretório em questão. Este comando adiciona TODOS os arquivos alterados à *staging area*. Para adicionar pastas ou arquivos especificos, utilize o nome do arquivo/pasta no lugar do ponto.

## Efetivando alterações (commit)
Com as alterações adicionadas à staging area, elas estão prontas para serem efetivas. O comando ``git commit`` torna as alterações persistentes no repositório.

````
git commit -m "Descrição da alteração"
````

A descrição da alteração é importante para organizar a versão e manter outros programadores cientes de que tipo de alterações foram realizadas naquele commit.

> NOTA: Adicionando mais um comando ``-m "descricao"`` ao final do código acima é possível introduzir também uma descrição mais completa ao commit realizado

## Branches

![bg fit left](../../Assets/img/git-branches.png)

Um branch é linha individual de histórico que contém arquivos que podem diferir de outras ramificações.

````javascript
// Cria um novo branch chamado developer
git branch developer
// Deleta um branch chamado developer
git branch -d developer
// Exibe os branchs ativos
git branch -a
// Altera o branch atual para developer
git checkout developer
// Cria um branch vazio chamado developer
git checkout --orphan developer
````

### Merge

O comando `merge` é responsável por incorporar as modificações de outro branch.

````javascript
// Incorpora as modificações do branch developer no master
git checkout master
git merge developer
````

## Git Remote

Uma das funções mais desejadas do git é a possibilidade de estender a utilização do repositório git para um endereço remoto, oferecendo um local centralizado para compartilhamento e gerencia do código do projeto. Através deste repositório remoto, todos os desenvolvedores locais podem enviar mudanças ao projeto (seus commits locais). Ferramentas como o GitLab e GitHub permitem ainda um gerenciamento mais aprofundado do projeto oferecendo ferramentas como Merge Requests, Wiki, CI/CD, Controle de Issues e afins. Estas ferramentas permitem uma integração maior com a comunidade do projeto e com outros tipos de contribuidores, incluíndo os usuários. Dessa forma, os repositórios remotos de git ajudaram a popularizar centenas de projetos de código fonte aberto.

### Clonando um repositório

Para clonar um repositório git já existente, utilizamos o comando: ``git clone [url]``

``git clone git@gitlab.com:rVenson/linguagemdeprogramacao.git``

``git clone https://gitlab.com/rVenson/linguagemdeprogramacao.git``

A diferença entre os dois comandos acima é que o primeiro utiliza o protoco SSH para transferência de arquivos. Através deste protocolo é possível configurar chaves públicas/privadas para incrementar a segurança do repositório. No segundo exemplo, o git utiliza o protocolo HTTPS, onde será necessário realizar autenticação por meio deste protocolo caso o repositório não seja público.

Também é possível clonar um repositório que esteja acessível no sistema de arquivos da máquina atual, como por exemplo: ``git clone /c/Users/rvenson/Documents/LinguagemDeProgramacao``

### Configurando repositório local

Ao realizar o clone de um repositório, o git geralmente realiza a configuração para futuras atualizações (envio e recebimento). No entanto, é possível que você deseje configurar seu proprio repositório local ou realizar ajustes adicionais no projeto. Dessa forma, o seguinte comando será útil: ``git remote add origin <servidor>``. Este comando permite adicionar um atalho para o link do servidor que queremos utilizar. É importante verificar que tipo de protocolo estamos utilizando no link <servidor>, como já visto na sessão anterior.

``git remote add origin https://gitlab.com/rVenson/linguagemdeprogramacao.git``

Neste exemplo, adicionaremos um atalho chamado **origin** ao endereço **https** especificado. Utilize o comando ``git remote -v`` para verificar os repositórios configurados.

### Atualizando o repositório

Para atualizar seu repositório git local com as alterações mais recentes do repositório remoto, podemos utilizar os seguintes comandos:

``git fetch``: este comando atualiza as referências do projeto local, no entanto, não executa nenhuma mudança
``git pull``: este comando atualiza as referências do projeto local e executa as mudanças (merge)

Exemplos:

``git pull`` ou ``git pull origin``

Enquanto o primeiro comando realiza automaticamente a atualização do repositório local,  o segundo especifica qual o atalho (alias), caso haja mais de um atalho configurado no repositório. De um modo geral, não teremos problemas caso não tenha sido realizada nenhuma alteração no repositório local. O Git procura fazer as alterações automaticamente. Caso existam arquivos em conflito, o usuário terá que realizar as alterações manualmente. Por este motivo, em um nível iniciante, é importante sempre manter o repositório atualizado antes de iniciar qualquer mudança no projeto local.

### Enviando alterações

Após incluir novos commits ao seu repositório local você pode unir estas alterações ao repositório remoto, para que outros contribuidores possam acessar. Basta utilizar o comando ``git push``.

Exemplos:

``git push`` ou ``git push origin`` ou ``git push origin master``

O primeiro comando envia as alterações para o repositório padrão configurado (origin). O segundo especifica qual o endereço (alias) do repositório remoto (neste caso **origin**). O terceiro especifica também qual dos branches será enviado ao respositório remoto. O git fará automaticamente a inclusão das alterações no servidor remoto, caso não exista nenhuma inconsistência. Caso existam, o usuário reverá realizar o ``fetch`` e resolver os arquivos em conflito antes de enviar as alterações.

## .gitignore

O arquivo `.gitignore`, geralmente presente na pasta raiz do projeto, é responsável por especificar os arquivos e pastas que não devem ser versionados pelo projeto. Este arquivo é de extrema importância para que arquivos residuais, temporários e builds não sejam integrados ao controle de versão.

Para utilizar o `gitignore`, basta criar um arquivo com este mesmo nome na pasta principal do projeto. Dentro do arquivo, deve-se especificar linha por linha quais arquivos, tipos de arquivos e pastas devem ser recusados pelo git.

````bash
# Ignora toda a pasta bin dentro do projeto
bin/

# Ignora todos os arquivos com final .import
*.import

# Ignora o arquivo project.config
project.config
````

Cada IDE geralmente possui conjunto de arquivos e pastas que podem ser ignoradas, para saber quais são estes arquivos, você pode consultar o site https://www.gitignore.io/ e gerar o arquivo .gitignore padrão.

## Outros Materiais

* Fracz - Exercícios Interativos ([link](https://gitexercises.fracz.com/))

* Git Kata ([link](https://github.com/praqma-training/git-katas))

* Git Branching ([link](https://learngitbranching.js.org/))

* Git Handbook ([link](https://guides.github.com/introduction/git-handbook/))

* Guia Prático ([link](https://rogerdudler.github.io/git-guide/index.pt_BR.html))

## Exercícios

19. Identifique a finalidade dos seguintes comandos:

````
a. git init
b. git config --global user.name "turing"
c. git add EXERCICIO.txt
d. git add .
e. git commit -m "Adicionado nova interface"
f. git commit
g. git reset --hard HEAD
h. cd Downloads
i. pwd
j. cd ..
k. ls
l. git pull
m. git push
n. git clone https://gitlab.com/rVenson/meurepositorio
````

20. Inicie um novo repositório git local e em seguida:

a) Crie um arquivo `README.md` contendo uma descrição do GIT;

b) Crie uma pasta chamada `img` e insira uma imagem do git dentro desta pasta;

c) Realize um novo commit com a descrição `Estado inicial`;

d) Altere o arquivo `README.md` e insira a imagem da etapa `B` dentro do arquivo;

> Utilize a sintaxe markdown ``![](link)``;

e) Realize um novo commit com a descrição `Inserida a imagem do git`;

f) Crie um novo branch a partir do `master` chamado `developer` e altere para este branch;

g) Crie um novo arquivo de texto chamado ``exe19`` contendo as respostas do exercícios 19;

h) Crie um novo commit chamado `Incluido Exercicio 19`;

i) Crie um novo branch em branco chamado `exercicios`;

j) Adicione apenas o arquivo ``exe19`` no stash e realize o commit chamado ``Exercicio 19``;

h) Crie um novo arquivo chamado ``AUTHOR.md`` e insira o seu nome completo;

i) Adicione o arquivo `AUTHOR.md` no stash e realize o commit com a descrição ``Finalizado exercicios``;

j) Realize o merge entre o branch `developer` e `exercicios`;

k) Delete o branch `exercicios`;

l) Compacte a pasta do repostório.

20. Escolha um repositório disponível no gitlab.com ou github.com e faça a clonagem do mesmo, identificando qual foi o autor do último commit realizado no projeto e a(s) linguagem(s) utilizadas.

22. Clone o repositório ``https://gitlab.com/rVenson/unesc-git-exercicios`` para a máquina local. Crie uma pasta chamada ``exercicios`` e dentro dela uma pasta para cada exercícios da disciplina (Ex.: ``exe01, exe02, exe03...``). Em seguida adicione a solução dos exercícios em suas respectivas pastas, junto com um arquivo adicional ``README.md`` contendo a definição do exercício.