# Aula 03 - Programando com JavaScript

## Manipulação de Arrays

### map

O método map permite invocar uma função para cada elemento do array. Este método retorna um novo array como resultado.

````javascript
var minhaCallback = function conta(texto){
    return texto.length
}

var lista = ["verde", "amarelo", "azul"]
console.log(lista.map(minhaCallback))
````

No exemplo, o método `map` itera sobre cada um dos itens do vetor e retorna um novo vetor com o tamanho de cada item.

### sort

O método sort permite criar um comparador para vetores. Caso não seja passado um callback, o sort retorna uma conversão padrão para string e numbers.

````javascript
var minhaCallback = function compara(a, b){
    if(a.length > b.length){
        return -1
    }

    if(a.length < b.length){
        return 1
    }

    return 0
}

var lista = ["verde", "amarelo", "azul"]
console.log(lista.sort(minhaCallback))
````

No exemplo, o método `sort` itera sobre os elementos do vetor, organizando-os de acordo com o retorno especificado no callback.

### filter

O método filter cria um array com todos os elementos que passaram no teste do callback.

````javascript
var minhaCallback = function filtro(item){
    return item[0] === 'a'
}

var lista = ["verde", "amarelo", "azul"]
console.log(lista.filter(minhaCallback))
````

No exemplo, os itens da lista são filtrados de acordo com o critério retornado pelo callback.

### reduce

O método reduce permite executar uma função callback sobre um vetor de dados, resultado em um acumulador final.

````javascript
var minhaCallback = function soma(resultado, texto){
    return resultado + texto.length
}

var lista = ["verde", "amarelo", "azul"]
console.log(lista.reduce(minhaCallback, 0))
````

No exemplo, os itens da lista são iterados e acumulados em um acumulador.

## Classes

A declaração de classes pode ser realizada da seguinte maneira:

````javascript
class Casa {
  constructor() {
  }

  endereco() {
    console.log('Olá Mundo');
  }
}

var minhaCasa = new Casa() //Instancia o objeto
minhaCasa.endereco() // Olá Mundo
````

> As classes no JavaScript, ao contrário de funções e variáveis, não são *hoisted*, o que significa que devem ser declaradas antes de sua utilização.

> Mais informações em [Classes](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Classes)

### Herança

````javascript
class Apartamento extends Casa {

}
````

### Métodos Estáticos

````javascript
class App {
  static log() {
    console.log('Olá mundo');
  }
}

App.log(); // Olá Mundo
````

## Declaração de Constantes e Variáveis

Duas novas formas de realizar declarações:

### Constantes

Constantes não podem ser reatribuidas, no entanto, é possível alterar o conteúdo de um objeto.

````javascript
const pi = 3.14;
pi = 3; //Erro

const usuario = { nome: 'Ramon' };
usuario.nome = 'Ramon Venson'; // Funciona (Mutação)
````

### Variáveis Let

A diferença entre a declaração de variáveis usando a palavra ``var`` para ``let``, é que a última é limitada apenas ao escopo em que foi declarada.

````javascript
function soma(a, b) {
  let c = a + b;
  console.log(c);
}

soma(2, 3); // 5
console.log(c); // undefined
````

## Arrow Functions

Arrow Functions são formas alternativas de declaração de funções. A principal característica em relação à ``function expression`` é que não possuem seu próprio ``this``.

````javascript
class App {
    const usuario = "rvenson"

    minhaArrow = () => {
        console.log(this.usuario)
    }

    minhaFuncao(){
        console.log(this.usuario)
    }
}

var app = new App()
app.minhaArrow()
app.minhaFuncao()
````

## Valor padrão

Também é possível definir um valor padrão para os argumentos de uma função usando o operador de atribuição

````javascript
function soma(a = 1, b = 2) {
  console.log(a + b);
}

soma(4); // 6
````

## Desestruturação

A desestruturação pode extrair dados de objetos e vetores diretamente de variáveis e constantes.

````javascript
const time = {
  nome: 'Criciúma',
  dados: {
    sigla: 'CEC',
    site: 'criciumaesporteclube.com.br'
  }
};

let { nome } = time;
console.log(nome); // Criciúma
let { dados: { site } } = time;
console.log(site); // criciumaesporteclube.com.br
````

````javascript
function showName({ nome }) {
  console.log(nome);
}

const usuario = { nome: 'Ramon' };
showName(usuario); // Ramon
````

````javascript
const numeros = [1, 2, 3];
let [a, b, c] = numeros;

console.log(a); // 1
console.log(b); // 2
console.log(c); // 3
````

> Ver mais em [Desestruturação](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Operators/Atribuicao_via_desestruturacao)

## Operadores Rest e Spread

É possivel utilizar os operadores ``rest`` e ``spread`` (que possuem a mesma sintaxe), servem para recuperar o conteúdo de objetos ou vetores.

### Rest

````javascript
const numeros = [1, 2, 3, 4, 5]

let [a, b, ...c] = numeros

console.log(a) // 1
console.log(b) // 2
console.log(c) // [3, 4, 5]
````

### Spread

````javascript
const usuario = {
  nome: 'ramon',
  empresa: 'unesc'
}

const novoUsuario = { ...usuario, nome: 'maradona' }

console.log(novoUsuario)
// { nome: 'maradona', empresa: 'unesc' }
````

## Literals

O literals utiliza o apóstrofo para concatenar strings à variáveis ou constantes. Além do apóstrofo, é preciso utilizar a sintaxe ``${}`` para compor a variável.

````javascript
const a = 3;
console.log(`O número é ${a}`)
````

## Atalho sintático

````javascript
const nome = 'Ramon'

const usuario = {
  nome // Mesma coisa que nome: nome
}

console.log(user.nome); // Diego
````

## Importação

````javascript
// App.js
export function soma(a, b) {
  return a + b;
}

export const usuario = {
  nome: 'Ramon',
  empresa: 'UNESC'
};

export default class App {
  static log() {
    console.log('Hey');
  }
}
````

````javascript
// Principal.js
import App from 'App'
App.log() // Hey
App.soma(1, 2)
````

````javascript
// OutroArquivo.js
import { soma, usuario } from 'App';
soma(1, 3); // 4
console.log(usuario.nome); // Ramon
````

## Promises

Promises são objetos que representam processos assíncronos dentro do script. Uma promisse deve ser iniciada a partir de uma nova função, contendo os valores ``resolve`` e ``reject``, que são chamados respectivamente no sucesso ou na falha da promessa.

````javascript
//Criando uma promessa
let promessa = new Promise(function (resolve, reject) {
  setTimeout(function () {
    if (Math.random() > 0.5) {
      console.log("Promessa resolvida")
      resolve()
    } else {
      console.log("Promessa rejeitada")
      reject()
    }
  }, 3000)
})
````

````javascript
//usando uma promessa
promessa.then(function () {
  document.getElementById("log").textContent = "finalizado"
}).catch(function () {
  document.getElementById("log").textContent = "erro"
}).finally(function () {
  console.log("Fim da promessa")
})
````

## Exercícios

15. Implemente duas classes de acordo com o código abaixo. A primeira deve ser uma classe chamada ``Gerador``, que possui um método chamado ``gerarHTML``, que retorna apenas um elemento ``div`` em branco, e um método chamado ``addLista``, que armazena na classe um array com a lista de compras. A segunda classe chama-se ``GeradorLista`` e deve estender a primeira classe, alterando o método ``gerarHTML`` para gerar uma lista à partir de um array.

````javascript
class Gerador{
    //implementacao
}

class GeradorLista extends Gerador{
    //implementação
}

var gerador = GeradorLista()
var listaCompras = ["shampoo", "frutas", "arroz"]
gerador.addLista(listaCompras)
gerador.geraHTML() 
// gera uma lista com os itens de listaCompras
````

16. Reescreva a função abaixo usando o operador rest/spread, map e arrow functions

````javascript
function soma(numeros) {
    var resultado = 0;
    for(var i = 0; i < numeros.length; i++){
        resultado += numeros[i]
    }
    return resultado;
}
````

17. Implemente um gerador de números de telefone. Para cada número inserido em uma lista na página, o usuário deve poder iniciar/encerrar uma ligação (fictícia).

18. Crie uma página que seja capaz de gerar novas promises e imprimir na tela uma lista com as promises iniciadas. Cada promise representa uma partida entre dois times de futebol digitados pelo usuário. Ao final do tempo de ``4.5 segundos``, o sistema atualiza o resultado da partida (aleatoriamente) na tela. Várias promises podem ser iniciadas simultaneamente.