/*
* O nome do personagem contenha pelo menos 3 letras
* O personagem não pode se chamar:
* Azul
* Violeta
* Verde
* Não podem haver magos em Venore ("Erro: Local perigoso para magos")
*/

function enviar(){
    var erro = ""
    
    var nome = document.getElementById("nome").value.toLowerCase()
    var classe = document.getElementById("classe").value.toLowerCase()
    var cidade = document.getElementById("cidade").value.toLowerCase()

    if(nome.length < 3) erro = "O nome do personagem deve conter ao menos 3 letras"
    if(nome == "azul" || nome == "violeta" || nome == "verde") erro = "Nome inválido. Escolha outro."
    if(classe == "mago" && cidade == "venore") erro = "Venore é perigosa para magos"
    if(nome == "") erro = "O nome deve ser preenchido"

    if(erro == ""){
        return true
    }
    
    alert(erro)
    return false
}