# CSS

____

## Conceitos

O Cascading Style Sheets (CSS) é uma linguagem de folhas de estilo, responsável por personalizar a aparência padrão dos componentes HTML no navegador.

Criado em 1996, o CSS encontra-se atualmente em sua versão padronizada 4, de 2017.

O CSS possui sintaxe simples, onde cada identificador (ou seletor) pode especificar um bloco de declaração com diferentes propriedades e valores.

___

````css
body
{
   font-family: Arial, Verdana, sans-serif;
   background-color: #FFF;
   margin: 5px 10px;
}
````

No exemplo acima, o seletor ``body``, que corresponde ao elemento ``<body>`` do HTML, possui três propriedades alteradas da sua aparência genérica: a família de fontes, a cor de fundo e a margem.

Todo par propriedade-atributo deve ser seguido do caracter ``;``

Dessa forma, o CSS permite organizar a estrutura de dados presente em um documento HTML de diferentes formas, procurando garantir a usabilidade e responsividade da página há diferentes ambientes.

___

## Sintaxe

Por padrão, o CSS permite a declaração de diferentes tipos de seletores:

### Elemento HTML

Identifica e define estilo para todos os elementos HTML declarados.

````css
div {
    background-color: tomato;
}
````

___

### Classe

Identifica e define estilo para todos os elementos que possuem o atributo ``class`` com mesmo nome (Ex.: ``class="minhaClasse"``)

````css
.minhaClasse {
    padding: 50px;
}
````

___

### ID

Identifica e define estilo o elemento com atributo ``id`` com mesmo nome (Ex.: ``id="TextoCPF"``)

````css
#textoCPF {
    color: #777;
}
````

___

## Incorporação

O CSS pode ser incorporado à um documento HTML de várias maneiras:

1. através da inclusão de um documento externo, através da tag ``link``;
````html
<link rel="stylesheet" href="estilo.css">
````

___

1. internamente no documento HTML, por meio da tag ``style``;
````html
<style>
    body {
        background-color: blue;
    }
</style>
````

1. ou de forma *inline*, utilizando o atributo ``style``, nos elementos HTML.
````html
 <h1 style="color:blue;">Título Azul</h1>
````

____

## Cheat Sheet

* [W3Schools](https://www.w3schools.com/css/)
* [Mozilla](https://developer.mozilla.org/pt-BR/docs/Web/CSS/)

____

| Propriedade | Descrição | Exemplo de Propriedade |
| --- | --- | --- |
| background-image | Define uma imagem de fundo | ``"image.png"`` |
| background-color | Define uma cor de fundo | ``tomato`` |
| border | Define opções de borda | ``5px solid white`` |
| padding | Define espaçamento interno do elemento | ``15px`` |
| margin | Define margem elemento | ``10px`` ou ``auto`` |
| font-family | Define família de fontes | ``Georgia, Times New Roman`` |
| font-size | Define o tamanho da fonte | ``1.5rem`` |
| color | Define a cor da fonte | ``white`` |
| width | Define largura do componente | ``100%`` |
| height | Define altura do componente | ``auto`` |
| display | Define o tipo de renderização dos componentes | ``flex`` |
