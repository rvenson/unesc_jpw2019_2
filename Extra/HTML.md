# HTML

____

## Conceitos

### Linguagem de Marcação

* O Hyper-Text Markup Language (HTML) é uma linguagem de marcação utilizada na construção de documentos de hipertexto para a Web.

* Foi proposta por Tim Berners Lee em 1990 junto com o protocolo HTTP e seu modelo cliente-servidor

* O HTML é interpretado por navegadores web por meio de tags, que estabelecem a estrutura dos dados inseridos em um documento.

* Atualmente o HTML encontra-se na versão 5.2 (Dezembro de 2017)

____

## Estrutura Básica

````html
<!DOCTYPE html>
<html>
    <head>
        <-- Cabeçalhos -->
    </head>
    <body>
        <-- Corpo da Página -->
    </body>
</html>
````

____

* Todo documento HTML (extensão ``.html``) possui uma estrutura aninhada que inicia pela declaração da tag ``html``, na raiz do documento. Essa tag é seguida pelas descrição dos dados do cabeçalho do documento (``head``)  e pelo corpo da página (``body``)

* A declaração da tag ``<!DOCTYPE html>`` é utilizada para que os navegadores reconheçam o tipo de documento recebido.

____

### Elementos

* Toda marcação é realizanda usando o padrão ``<tag>conteudo</tag>``. Em alguns casos também é possível usar elementos sem nenhum conteúdo, como em ``<tag>``, os chamados elementos vázios.

> Nota: No padrão XHTML (descontinuado), os elementos vazios necessivam ser fechados com ``/``, como em ``<tag />``. Atualmente, esta condição é facultativa.

____

### Atributos

* Alguns elementos do HTML permitem ainda a declaração de atributos internos, que possuem a finalidade de especificar personalizações no comportamento padrão do elemento HTML.

````html
 <a href="https://meusite.com">Visite meu site!</a>
````

____

> Exemplo 01 [ver](../Exemplos/Exemplo02.html)
````html
<!DOCTYPE html>
<html>
    <head>
        <title>Minha homepage</title>
    </head>
    <body>
        <h1>Bem-vindo à minha página pessoal!</h1>
        <p>
            Este sou eu:
            <img src="img/foto.png" alt="Foto pessoal">
        </p>
        <p>
            Sejam bem-vindos ao meu site pessoal.
            Aqui vocês encontraram dicas de
            <b>computação</b> e <b>culinária</b>.
        </p>
    </body>
</html>
````
____

![](../Assets/img/Exemplo01.png)

____

## Cheat Sheet

* [W3Schools](https://www.w3schools.com/tags)
* [Mozilla](https://developer.mozilla.org/pt-BR/docs/Web/HTML/Element)

____

### Primárias

Os principais elementos que compõe um documento HTML

| Elemento | Descrição |
| --- | --- |
| html | Define a raiz dos elementos html em um documento |
| head | Metadados do documento |
| body | Corpo de conteúdo do documento |

____

### Semânticas

Elementos que geralmente não são estilizados pelo navegador, mas possuem função semântica para outros interpretadores (como folha de estilos e leitores de tela)

| Elemento | Descrição |
| --- | --- |
| article | Conteúdo que pode ser apresentado de forma independente |
| aside | Conteúdo apresentado nas laterais da página |
| footer | Conteúdo apresentado no rodapé da página |
| header | Conteúdo apresentado na parte superior da página |
| main | Conteúdo principal da página |
| nav | Seção da página com links para navegação |
| section | Seção genérica |

____

### Textuais

| Elemento | Descrição |
| --- | --- |
| p | Parágrafo |
| li | Lista |
| ul | Item de Lista |
| div | Container de conteúdo genérico |
| a | Hiperlink |
| b | Negrito |
| i | Itálico |
| br | Quebra de Linha |

____

### Multimídia

| Elemento | Descrição |
| --- | --- |
| audio | Áudio |
| img | Imagem |
| video | Vídeo |
| source | Define um item de multimídia |

____

### Tabelas

| Elemento | Descrição |
| --- | --- |
| table | Tabela |
| tr | Linha da tabela |
| td | Célula da tabela |
| thead | Cabeçalho da célula |

____

### Formulários

| Elemento | Descrição |
| --- | --- |
| form | Corpo de formulário |
| input | Componente para entrada de dados |
| textarea | Área de texto editável |
| button | Botão |
| select | Seletor de entrada |
| option | Elementos de um seletor |

____

### Outras

| Elemento | Descrição |
| --- | --- |
| title | Define o título do documento |
| hr | Linha horizontal |

____

> Exemplo 02 [ver](../Exemplos/Exemplo02.html)

____

## Criando Formulários

Um dos principais recursos do HTML é a possibilidade de inserir elementos interativos que permitem ao usuário submeter informação aos controladores do serviço web (ou ao próprio front-end)

A criação de uma entrada de dados é feita geralmente utilizando a tag ``form``, que agrupa elementos de formulário com propriedades em comum.

____

````html
<form action="cadastro.php">
    Usuário: <input type="text" name="usuario">
    Senha: <input type="text" name="senha">

    <button type="submit">Login</button>
</form>
````

____

O formulário contém um atributo chamado ``action``, que descreve o endereço para onde serão enviados os dados dos itens do formulário. Cada um dos elementos do formulário são identificados por meio do atributo ``name``.

Um elemento ``button`` ou ``input`` com o atributo ``type=submit`` deve ser criado dentro do formulário para a função de envio dos dados.

> O método HTTP padrão para envio dos dados é o GET

____

### Tipos de Inputs

* Button
* Checkbox
* Color
* Date
* Email
* File
* Radio
* Text
* Password

Lista completa [aqui](https://developer.mozilla.org/pt-BR/docs/Web/HTML/Element/Input)
