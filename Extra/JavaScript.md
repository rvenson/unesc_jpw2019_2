# JavaScript

____

## Conceitos

O JavaScript é uma linguagem de programação de alto nível suportada em navegadores e, atualmente, fortemente utilizada em server-side.

Apesar da semelhança no nome, o JavaScript possui paradigmas e design extremamente diferentes da linguagem Java.

____

## Estrutura

O JavaScript pode ser executado pelo navegador a partir de um documento HTML utilizando-se do elemento ``script``, ou através da incorporação de um documento externo, usando a tag ``link``

````html
<script type="text/javascript" src="hello_world.js"></script>

ou

<script type="text/javascript">
    alert("Olá mundo!")
</script>
````

____

### Recursos

* Criação de Variáveis e Constantes

````javascript
var nome
const nome
let nome //Apenas ES6
````

> Enquanto a declaração através do ``var`` é atrelada ao escopo de uma função, a declaração utilizando o ``let`` é limitada apenas ao escopo do bloco onde foi declarada

____

### Estruturas Condicionais

* IF e ELSE

````javascript
if(valor){
    //Se verdadeiro
} else {
    //Se falso
}
````

____

### Estruturas de Repetição

* FOR

````javascript
for (var index = 0; index < 50; index++) {
    //iteração
}
````

____

* FOR-In

````javascript
for(const item in objeto){
    //iteração
}
````

____

* FOR-of

````javascript
for (var iterador of vetor) {
    //iteração
}
````

____

* WHILE

````javascript
while(condicao) {
    //iteração
}
````

____

### Funções

Para criar funções no JavaScript, basta utilizar a seguinte sintaxe:

````javascript
function nomeDaFuncao(parametro){
    //ações
}

//para chamara função, basta utilizar
nomeDaFuncao(parametro)
````

> Há outras formas de criar funções no padrão ES6, como as Arrow Functions, que serão vistas adiante

____

## Javascript/HTML

````javascript
//Recebe um elemento HTML correspondente ao ID passado
var item = document.getDocumentById("meuID")

//Exibe uma mensagem de alerta
alert("Minha mensagem de alerta!")

//Estiliza a cor de um elemento com o ID passado
document.getElementById("texto").style.color = "red";

//Altera o conteúdo de um elemento HTML
document.getElementById("telefone").innerHTML = "Olá mundo";
````
